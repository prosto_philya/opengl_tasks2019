# Install script for directory: /Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/libassimpd.4.1.0.dylib"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/libassimpd.4.dylib"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.4.1.0.dylib"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.4.dylib"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      execute_process(COMMAND "/usr/bin/install_name_tool"
        -id "/usr/local/lib/libassimpd.4.dylib"
        "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/libassimpd.dylib")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.dylib" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.dylib")
    execute_process(COMMAND "/usr/bin/install_name_tool"
      -id "/usr/local/lib/libassimpd.4.dylib"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.dylib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libassimpd.dylib")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/anim.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/ai_assert.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/camera.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/color4.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/color4.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/cmake-build-debug/external/Assimp/code/../include/assimp/config.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/defs.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Defines.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/cfileio.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/light.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/material.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/material.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/matrix3x3.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/matrix3x3.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/matrix4x4.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/matrix4x4.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/mesh.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/postprocess.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/quaternion.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/quaternion.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/scene.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/metadata.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/texture.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/types.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/vector2.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/vector2.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/vector3.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/vector3.inl"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/version.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/cimport.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/importerdesc.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Importer.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/DefaultLogger.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/ProgressHandler.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/IOStream.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/IOSystem.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Logger.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/LogStream.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/NullLogger.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/cexport.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Exporter.hpp"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/DefaultIOStream.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/DefaultIOSystem.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/SceneCombiner.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Compiler/pushpack1.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Compiler/poppack1.h"
    "/Users/simon/simon_bitbucket/opengl_tasks2019/external/Assimp/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

