#version 330


uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform float maxHeight;
uniform float minHeight;

layout(location = 0) in vec3 vertexPosition;

out vec4 color;


vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
    float h =  (vertexPosition.z - minHeight) / (maxHeight- minHeight);
    vec3 hsv = vec3((1.0 - h)*(180.0/255), 1.0,  0.5);
    color.a = h;
    color.rgb = hsv2rgb(hsv);
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}


