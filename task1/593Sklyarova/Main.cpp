#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"

#include <iostream>
#include <vector>


#include <glm/glm.hpp>

#include <stdlib.h>
#include <algorithm>
#include <SOIL2.h>



using namespace std;


class HeightMap {
public:

    vector<float> heightMap;
    int width;
    int height;
    int channels;

    float maxHeight;
    float minHeight;

    HeightMap(const char* filename, float maxH, float minH) {

        this->maxHeight = maxH;
        this->minHeight = minH;


        unsigned char* htMap = SOIL_load_image
                (
                        filename,
                        &width, &height, &channels,
                        SOIL_LOAD_L
                );

        for (int i = 0; i < width*height; i++) {
            heightMap.push_back((float)(htMap[i]));
        }

        float maxHeight = *( max_element(std::begin(heightMap), std::end(heightMap)) );
        float minHeight = *( min_element(std::begin(heightMap), std::end(heightMap)) );

        for (int i = 0; i < width*height; i++) {
            heightMap[i] = minH + (maxH - minH) * (heightMap[i] - minHeight) / (maxHeight- minHeight);
        }

    }

};


struct Vertex {
    glm::vec3 coords;
    glm::vec3 normal;

    Vertex() {
        coords = glm::vec3(0.0, 0.0, 0.0);
        normal = glm::vec3(0.0, 1.0, 0.0);
    }
    Vertex(int i, int j, float height, float step) {
        coords.x = j * step;
        coords.y = height;
        coords.z = i * step;
    }


};

class Terrain {
private:
    int width;
    int length;
    float step;
    float minHeight;
    float maxHeight;

    Vertex** vertices;



    void computeNormals() {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                vertices[i][j].normal = glm::vec3(0.0, 0.0, 0.0);

                glm::vec3 up, down, right, left;
                if (i > 0) {
                    up = glm::vec3(0.0, vertices[i - 1][j].coords.y - vertices[i][j].coords.y, -1.0);
                }
                if (i < length - 1) {
                     down = glm::vec3(0.0, vertices[i + 1][j].coords.y - vertices[i][j].coords.y, 1.0);
                }
                if (j > 0) {
                    left = glm::vec3(-1.0, vertices[i][j - 1].coords.y - vertices[i][j].coords.y, 0.0);
                }
                if (j < width - 1) {
                    right = glm::vec3(1.0, vertices[i][j + 1].coords.y - vertices[i][j].coords.y, 0.0);
                }

                if (j > 0 && i > 0) {
                    vertices[i][j].normal += glm::normalize(glm::cross(up, left));
                }
                if (j > 0 && i < length - 1) {
                    vertices[i][j].normal += glm::normalize(glm::cross(left, down));
                }
                if (j < width - 1 && i < length - 1) {
                    vertices[i][j].normal += glm::normalize(glm::cross(down, right));
                }
                if (j < width - 1 && i > 0) {
                    vertices[i][j].normal += glm::normalize(glm::cross(right, up));
                }

                vertices[i][j].normal = glm::normalize(vertices[i][j].normal);

            }
        }
    }

public:


    Terrain(HeightMap* heightMap,  float step) {
        width = heightMap->width;
        length = heightMap->height;
        this->step = step;
        minHeight = heightMap->minHeight;
        maxHeight = heightMap->maxHeight;

        vertices = new Vertex*[length];
        for (int i = 0; i < length; i++) {
            vertices[i] = new Vertex[width];
            for (int j = 0; j < width; j++) {
                vertices[i][j] = Vertex(i, j, (heightMap->heightMap[i * width + j]), step);
            }
        }

        computeNormals();
    }

    ~Terrain() {
        for (int i = 0; i < length; i++) {
            delete[] vertices[i];
        }
        delete[] vertices;
    }

    int getWidth() {
        return width;
    }

    int getLength() {
        return length;
    }

    float getMaxHeight(){
        return maxHeight;
    }

    float getMinHeight(){
        return minHeight;
    }

    MeshPtr getMesh()
    {
        std::vector<glm::vec3> verts;
        std::vector<glm::vec3> normals;


        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < width - 1; j++) {

                glm::vec3 normal = vertices[i][j].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i][j].coords);


                normal = vertices[i][j + 1].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i][j + 1].coords);

                normal = vertices[i + 1][j].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i + 1][j].coords);

                normal = vertices[i][j + 1].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i][j + 1].coords);

                normal = vertices[i + 1][j].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i + 1][j].coords);

                normal = vertices[i + 1][j + 1].normal;
                normals.push_back(normal);
                verts.push_back(vertices[i + 1][j + 1].coords);


            }
        }

        /////////////////////////////////////////
        for(int i = 0; i < verts.size(); i++){
            swap(verts[i].y,  verts[i].z);
            swap(normals[i].y,  normals[i].z);
        }
        ////////////////////////////////////////

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(verts.size() * sizeof(float) * 3, verts.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(verts.size());


        return mesh;
    }
};






class SampleApplication : public Application
{
public:
    Terrain* Terr;
    MeshPtr _terrain;

    ShaderProgramPtr _shader;

    SampleApplication(Terrain* terrain) {
        Terr = terrain;
    }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();
        _terrain  = Terr->getMesh();
        _terrain->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, -10.0f, 0.0f)));


        _shader = std::make_shared<ShaderProgram>("593SklyarovaData1/colorHeight.vert", "593SklyarovaData1/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



        _shader->use();


        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


        _shader->setMat4Uniform("modelMatrix", _terrain->modelMatrix());
        _shader->setFloatUniform("maxHeight", Terr->getMaxHeight());
        _shader->setFloatUniform("minHeight", Terr->getMinHeight());

        _terrain->draw();

    }
};




int main()
{
    float step = 0.15;
    HeightMap _heightMap = HeightMap("593SklyarovaData1/tamriel.jpg", 1, 0);
    Terrain* _terrain = new Terrain(&_heightMap, step);

    SampleApplication app(_terrain);
    app.start();

    return 0;
}