include_directories(${PROJECT_SOURCE_DIR}/task1/593Sklyarova/593SklyarovaData1/ )
set(SRC_FILES
        Main.cpp
        ${PROJECT_SOURCE_DIR}/task1/593Sklyarova/593SklyarovaData1/Application.cpp
	${PROJECT_SOURCE_DIR}/task1/593Sklyarova/593SklyarovaData1/DebugOutput.cpp
    	${PROJECT_SOURCE_DIR}/task1/593Sklyarova/593SklyarovaData1/Camera.cpp
    	${PROJECT_SOURCE_DIR}/task1/593Sklyarova/593SklyarovaData1/Mesh.cpp
    	${PROJECT_SOURCE_DIR}/task1/593Sklyarova/593SklyarovaData1/ShaderProgram.cpp
        )

MAKE_OPENGL_TASK(593Sklyarova 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(593Sklyarova stdc++fs)
endif()