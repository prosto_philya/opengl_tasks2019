cmake_minimum_required(VERSION 3.0)

set(SRC_FILES
        Main.cpp
        PerlinNoise.hpp
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/DebugOutput.cpp

        common/Application.hpp
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        )

add_definitions(-DGLM_ENABLE_EXPERIMENTAL)

MAKE_OPENGL_TASK(591Kozlov 1 "${SRC_FILES}")