//
// Created by Filipp Petrov on 2019-04-13.
//

#ifndef PROJECT_GET_MESH_H
#define PROJECT_GET_MESH_H

#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include <Mesh.hpp>

MeshPtr CreateTreeMesh(const int maxLevel=5);

void CreateCone(
        const glm::vec3& a,
        const glm::vec3& b,
        float r1,
        float r2,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals
);

#endif //PROJECT_GET_MESH_H
