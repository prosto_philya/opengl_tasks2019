cmake_minimum_required(VERSION 3.0)
set (SRC_FILES
    Main.cpp
        get_mesh.h
        get_mesh.cpp
        tree.h
        tree.cpp
    common/Application.cpp
    common/DebugOutput.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/Application.hpp
    common/DebugOutput.h
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_C_COMPILER clang)

include_directories(common)

MAKE_OPENGL_TASK(594Petrov 1 "${SRC_FILES}")
