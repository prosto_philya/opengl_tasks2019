#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <SOIL2.h>
#include <cmath>


void addIndices(std::vector<unsigned int> &indices, int &p) {
    indices.push_back(p);
    indices.push_back(p+1);
    indices.push_back(p+2);
    indices.push_back(p+2);
    indices.push_back(p+3);
    indices.push_back(p+1);
    p += 4;
}

int
getHeightmapPoint(unsigned char *ht_map, int w, int x, int y) {
    return ht_map[(x + y * w)];
}

MeshPtr
makeGround() {
    /* load an image as a heightmap, forcing greyscale (so channels should be 1) */
    int width, height, channels;
    unsigned char *ht_map = SOIL_load_image
        (
            "593YakimushkinaData1/terrain2.jpg",
            &width, &height, &channels,
            SOIL_LOAD_L
        );
    std::cout << width << " : " << height << " : "<< channels << std::endl;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normal;
    std::vector<unsigned int> indices;

    float step = 0.1;
    int p = 0;

    for( int z = 0; z < width-1; z += 1 ) {
        for( int x = 0; x < height-1; x += 1 ) {

            float y1 = getHeightmapPoint(ht_map, width, x, z) / 255.0f;
            float y2 = getHeightmapPoint(ht_map, width, x, z+1) / 255.0f;
            float y3 = getHeightmapPoint(ht_map, width, x+1, z) / 255.0f;
            float y4 = getHeightmapPoint(ht_map, width, x+1, z+1) / 255.0f;

            float xpos = x * step;
            float zpos = z * step;

            glm::vec3 v1(     xpos, zpos,      y1);
            glm::vec3 v2(     xpos, zpos+step, y2);
            glm::vec3 v3(xpos+step, zpos,      y3);
            glm::vec3 v4(xpos+step, zpos+step, y4);

            vertices.push_back(v1);
            vertices.push_back(v2);
            vertices.push_back(v3);
            vertices.push_back(v4);

            glm::vec3 a = v1 - v2;
            glm::vec3 b = v2 - v3;
            glm::vec3 c = v3 - v4;

            glm::vec3 n1 = glm::normalize(glm::cross(a, b));
            glm::vec3 n2 = glm::normalize(glm::cross(b, c));

            normal.push_back(n1);
            normal.push_back(n1);
            normal.push_back(n2);
            normal.push_back(n2);
            addIndices(indices, p);
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    buf1->setData(normal.size() * sizeof(float) * 3, normal.data());

    DataBufferPtr buf_indices = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
    buf_indices->setData(indices.size() * sizeof(unsigned int), indices.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setIndices(indices.size(), buf_indices);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

class SampleApplication : public Application
{
public:
    MeshPtr _ground;

    ShaderProgramPtr _shader1;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _ground = makeGround();
        
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, -10.0f, 0.0f)));

        //Создаем шейдерную программу
        _shader1 = std::make_shared<ShaderProgram>("593YakimushkinaData1/terrain.vert", "593YakimushkinaData1/terrain.frag");
    }

    void update() override
    {
        Application::update();
    }

    void draw() override
    {
        Application::draw();
        
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0.2f,0.3f,0.4f,1.0f);

        //Устанавливаем шейдер.
        _shader1->use();
        //Устанавливаем общие юниформ-переменные
        _shader1->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader1->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader1->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        _ground->draw();
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        //_ground->draw();
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}