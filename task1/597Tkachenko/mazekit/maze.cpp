#include "maze.hpp"


namespace mazekit {

Maze::Maze(const std::string& filepath) {
	std::fstream stream(filepath, std::ios::in);
    stream >> height_ >> width_;
	mazeCells_.assign(height_, std::vector<bool>(width_, false));
    verticalWalls_.assign(height_, std::vector<bool>(width_ + 1, false));
    horizontalWalls_.assign(height_ + 1, std::vector<bool>(width_, false));
    angleWalls_.assign(height_ + 1, std::vector<bool>(width_ + 1, false));
    char tmp;
    for (int index = 0; index <= 2 * height_; ++index) {
        for (int second = 0; second <= 2 * width_; ++second) {
            stream >> tmp;
            const int verticalCellNumber = index / 2;
            const int horizontalCellNumber = second / 2;
            const int verticalWall = index % 2;
            const int horizontalWall = second % 2;
            if (verticalWall && horizontalWall) {
                if (tmp == 'X') {
                    mazeCells_[verticalCellNumber][horizontalCellNumber] = true;
                }
            } else if (verticalWall && !horizontalWall) {
                if (tmp == '|') {
                    verticalWalls_[verticalCellNumber][horizontalCellNumber] = true;
                }
            } else if (horizontalWall && !verticalWall) {
                if (tmp == '-') {
                    horizontalWalls_[verticalCellNumber][horizontalCellNumber] = true;
                }
            } else {
                if (tmp == 'X') {
                    mazeCells_[verticalCellNumber][horizontalCellNumber] = true;
                }
            }
        }
    }
	stream.close();
    updateWalls();
}

void Maze::updateWalls() {
    for (size_t y = 0; y < verticalWalls_.size(); ++y) {
        for (size_t x = 0; x < verticalWalls_[0].size(); ++x) {
            updateVerticalWall(y, x);
        }
    }
    for (size_t y = 0; y < horizontalWalls_.size(); ++y) {
        for (size_t x = 0; x < horizontalWalls_[0].size(); ++x) {
            updateHorizontalWall(y, x);
        }
    }
    for (size_t y = 0; y <= height_; ++y) {
        for (size_t x = 0; x <= width_; ++x) {
            updateAngleWall(y, x);
        }
    }
}

void Maze::updateVerticalWall(int y, int x) {
    if (x > 0) {
        if (mazeCells_[y][x - 1]) {
            verticalWalls_[y][x] = true;
        }
    }
    if (x < width_) {
        if (mazeCells_[y][x]) {
            verticalWalls_[y][x] = true;
        }
    }
}

void Maze::updateHorizontalWall(int y, int x) {
    if (y > 0) {
        if (mazeCells_[y - 1][x]) {
            horizontalWalls_[y][x] = true;
        }
    }
    if (y < height_) {
        if (mazeCells_[y][x]) {
            horizontalWalls_[y][x] = true;
        }
    }
}

void Maze::updateAngleWall(int y, int x) {
    if (y > 0) {
        if (verticalWalls_[y - 1][x]) {
            angleWalls_[y][x] = true;
        }
    }
    if (y < height_) {
        if (verticalWalls_[y][x]) {
            angleWalls_[y][x] = true;
        }
    }
    if (x > 0) {
        if (horizontalWalls_[y][x - 1]) {
            angleWalls_[y][x] = true;
        }
    }
    if (x < width_) {
        if (horizontalWalls_[y][x]) {
            angleWalls_[y][x] = true;
        }
    }
}
	
}