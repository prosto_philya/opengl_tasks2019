#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec3 color;

void main()
{
    // высота была нормирована не от 0 до 1, отнормируем ее от 0 до 1
    float z = vertexPosition.z * 100.0f / 255.0f;

    // (255, 0, 81) - алый цвет будет на максимумах высоты
    // (0, 255, 0) - зеленый цвет будет на минимумах высоты
    color = vec3(z, (1 - z), z * 81.0f / 255.0f);
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}