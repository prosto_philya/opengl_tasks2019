#include "MazeCameraMover.hpp"

#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <glm/ext.hpp>
#include <Mesh.hpp>

#include <iostream>
#include <fstream>
#include <vector>

/**
Проект №3: Лабиринт
*/

class MazeApplication : public Application {
protected:
    MeshPtr mazeMesh;
    Maze* localMaze;
    ShaderProgramPtr _shader;
    std::shared_ptr<CameraMover> _camera_active;
    std::shared_ptr<CameraMover> _camera_inactive;

public:
    MazeApplication(Maze &maze);

    ~MazeApplication() {}

    void makeScene() override {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        mazeMesh = makeMaze(*localMaze);
        mazeMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.5f)));

        _shader = std::make_shared<ShaderProgram>(
                "591EgorovData1/simple.vert",
                "591EgorovData1/simple.frag");
    }
    void update() override {
        double dt = glfwGetTime() - _oldTime;

        glm::mat4 matrix = mazeMesh->modelMatrix();

        mazeMesh->setModelMatrix(matrix);

        Application::update();
    }
    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", mazeMesh->modelMatrix());
        mazeMesh->draw();

    }
    void handleKey(int key, int scancode, int action, int mods)
    {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_C)
            {
                _camera_active.swap(_camera_inactive);
                _cameraMover = _camera_active;
            }
        }
        Application::handleKey(key, scancode, action, mods);
    }
};

MazeApplication::MazeApplication(Maze &maze)
        : Application(std::make_shared<MazeCameraMover>(maze)),
          _camera_active(_cameraMover),
          _camera_inactive(std::make_shared<MazeCameraMover>(maze, true)) {
    localMaze = &maze;
}

int main() {
    Maze maze("591EgorovData1/mazeSample");
    MazeApplication app(maze);
    app.start();

    return 0;
}
