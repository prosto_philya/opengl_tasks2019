#include "BreatherSurface.hpp"
#include <iostream>

void BreatherSurface::draw() {
    Application::draw();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader_->use();
    shader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    shader_->setMat4Uniform("modelMatrix", surface_->modelMatrix());

    surface_->draw();
}

void BreatherSurface::makeScene() {
    Application::makeScene();

    _cameraMover = std::make_shared<FreeCameraMover>();
    
    makeSurface();

    shader_ = std::make_shared<ShaderProgram>();
    shader_->createProgram("./592TverdovData1/shaderNormal.vert", "./592TverdovData1/shader.frag");
}

void BreatherSurface::makeSurface() {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    float u_bound_left = 0;
    float u_bound_right = 10;
    float v_bound_left = -37.4;
    float v_bound_right = 37.4; 

    for (unsigned int i = 0; i < detail_; i++) {

        float u1 = u_bound_left + (u_bound_right - u_bound_left) * i / detail_;
        float u2 = u_bound_left + (u_bound_right - u_bound_left) * (i + 1) / detail_;

        for (unsigned int j = 0; j < detail_; j++) {
            float v1 = v_bound_left + (v_bound_right - v_bound_left) * j / detail_;
            float v2 = v_bound_left + (v_bound_right - v_bound_left) * (j + 1) / detail_;

            vertices.push_back(get_coors(u1, v1));
            vertices.push_back(get_coors(u1, v2));

            vertices.push_back(get_coors(u1, v1));
            vertices.push_back(get_coors(u2, v1));
            
            vertices.push_back(get_coors(u1, v1));
            vertices.push_back(get_coors(u2, v2));
            
            vertices.push_back(get_coors(u1, v2));
            vertices.push_back(get_coors(u2, v1));

            vertices.push_back(get_coors(u1, v2));
            vertices.push_back(get_coors(u2, v2));
            
            vertices.push_back(get_coors(u2, v1));
            vertices.push_back(get_coors(u2, v2));
            
            normals.push_back(get_normal(u1, v1));
            normals.push_back(get_normal(u1, v2));

            normals.push_back(get_normal(u1, v1));
            normals.push_back(get_normal(u2, v1));
            
            normals.push_back(get_normal(u1, v1));
            normals.push_back(get_normal(u2, v2));
            
            normals.push_back(get_normal(u1, v2));
            normals.push_back(get_normal(u2, v1));

            normals.push_back(get_normal(u1, v2));
            normals.push_back(get_normal(u2, v2));
            
            normals.push_back(get_normal(u2, v1));
            normals.push_back(get_normal(u2, v2));          
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    surface_ = std::make_shared<Mesh>();
    surface_->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    surface_->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    surface_->setPrimitiveType(GL_LINES);
    surface_->setVertexCount(vertices.size());
    surface_->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
}

void BreatherSurface::handleKey(int key, int scancode, int action, int mods){
    Application::handleKey(key, scancode, action, mods);

    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_MINUS) {
            detail_ -= 10;
            if (detail_ < MIN_DETAIL) {
                detail_ = MIN_DETAIL;
            }
        }
        if (key == GLFW_KEY_EQUAL) {
            detail_ += 10;
        }

        makeSurface();

        Application::update();
    }
}

float BreatherSurface::get_denom(const float u, const float v) {
    return aa_ * (w_ * w_ * cosh(aa_ * u) * cosh(aa_ * u) + aa_ * aa_ * sin(w_ * v) * sin(w_ * v));
}

float BreatherSurface::get_x(const float u, const float v) {
    return -u + 2 * w_ * w_ * cosh(aa_ * u) * sinh(aa_ * u) / get_denom(u, v);
}

float BreatherSurface::get_y(const float u, const float v) {
    return 2 * w_ * cosh(aa_ * u) * ((-1) * w_ * cos(v) * cos(w_ * v) - sin(v) * sin(w_ * v)) / get_denom(u, v);     
}

float BreatherSurface::get_z(const float u, const float v) {
    return 2 * w_ * cosh(aa_ * u) * ((-1) * w_ * sin(v) * cos(w_ * v) + cos(v) * sin(w_ * v)) / get_denom(u, v);
}

glm::vec3 BreatherSurface::get_coors(const float u, const float v) {
    return glm::vec3(get_x(u, v), get_y(u, v), get_z(u, v));
}


float BreatherSurface::get_der_x_u(const float u, const float v) {
    return (-1) + 2 * w_ * w_ * aa_ * (cosh(aa_ * u) * cosh(aa_ * u) + sinh(aa_ * u) * sinh(aa_ * u )) / get_denom(u, v) - 
        4 * w_ * w_ * cosh(aa_ * u) * sinh(aa_ * u) * aa_ * aa_ * w_ * w_ * cosh(aa_ * u ) * sinh(aa_ * u ) / 
        get_denom(u, v) / get_denom(u, v);
}

float BreatherSurface::get_der_y_u(const float u, const float v) {
    return (-2) * w_ * aa_ * sinh(aa_ * u) * (w_ * cos(v) * cos(w_ * v) + sin(v) * sin(w_ * v)) / get_denom(u, v) +
        + 4 * w_ * cosh(aa_ * u) * (w_ * cos(v) * cos(w_ * v) + sin(v) * sin(w_ * v)) * 
        aa_ * aa_ * w_ * w_ * cosh(aa_ * u) * sinh(aa_ * u) / get_denom(u, v) / get_denom(u, v);
}

float BreatherSurface::get_der_z_u(const float u, const float v) {
    return 2 * w_ * aa_ * sinh(aa_ * u ) * ((-1) * w_ * sin(v) * cos(w_ * v) + cos(v) * sin(w_ * v)) / get_denom(u, v) -
        4 * w_ * cosh(aa_ * u) * ((-1) * w_ * sin(v) * cos(w_ * v) + cos(v) * sin(w_ * v)) * aa_ * aa_ * w_ * w_ * 
        cosh(aa_ * u) * sinh(aa_ * u) / get_denom(u, v) / get_denom(u, v);
}

float BreatherSurface::get_der_x_v(const float u, const float v) {
    return (-4) * w_ * w_ * cosh(aa_ * u) * sinh(aa_ * u) * w_ * aa_ * aa_ * aa_ * sin(w_ * v) * cos(w_ * v ) / 
        get_denom(u, v) / get_denom(u, v);
}

float BreatherSurface::get_der_y_v(const float u, const float v) {
    return (-2) * w_ * cosh(aa_ * u) * (w_ * sin(v) * cos(w_ * v) + cos(v) * sin(w_ * v) - 
        w_ * w_ * cos(v) * sin(w_ * v) - w_ * sin(v) * cos(w_ * v)) / get_denom(u, v) + 
        + 4 * w_ * cosh(aa_ * u) * (w_ * cos(v) * cos(w_ * v) + 
        sin(v) * sin(w_ * v)) * aa_ * aa_ * aa_ * w_ * sin(w_ * v) * cos(w_ * v) / get_denom(u, v) / get_denom(u, v);
}

float BreatherSurface::get_der_z_v(const float u, const float v) {
    return 2 * w_ * cosh(aa_ * u) * ((-1) * w_ * cos(v) * cos(w_ * v) + w_ * w_ * sin(v) * sin(w_ * v) - 
        sin(v) * sin(w_ * v) + cos(v) * w_ * cos(w_ * v)) / get_denom(u, v) -
        4 * w_ * cosh(aa_ * u) * ((-1) * w_ * sin(v) * cos(w_ * v) + cos(v) * sin(w_ * v)) * 
        aa_ * aa_ * aa_ * w_ * sin(w_ * v) * cos(w_ * v) / get_denom(u, v) / get_denom(u, v);
}

glm::vec3 BreatherSurface::get_der_u(const float u, const float v) {
    return glm::vec3(get_der_x_u(u, v), get_der_y_u(u, v), get_der_z_u(u, v));
}

glm::vec3 BreatherSurface::get_der_v(const float u, const float v) {
    return glm::vec3(get_der_x_v(u, v), get_der_y_v(u, v), get_der_z_v(u, v));
}


glm::vec3 BreatherSurface::get_normal(const float u, const float v) {
    return glm::normalize(glm::cross(get_der_u(u, v), get_der_v(u, v)));
}
