#pragma once

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>


class BreatherSurface : public Application {
    public:
        MeshPtr surface_;
        ShaderProgramPtr shader_;
        
        void makeSurface();

        void makeScene() override;
        void draw() override;
        void handleKey(int key, int scancode, int action, int mods) override;

    private:
        float aa_ = 0.4;
        float w_ = sqrt(1 - aa_ * aa_);
        int detail_ = 100;
        const int MIN_DETAIL = 10;

        float get_denom(const float u, const float v);
        float get_x(const float u, const float v);
        float get_y(const float u, const float v);
        float get_z(const float u, const float v);
        glm::vec3 get_coors(const float u, const float v);


        float get_der_x_u(const float u, const float v);
        float get_der_y_u(const float u, const float v);
        float get_der_z_u(const float u, const float v);

        float get_der_x_v(const float u, const float v);
        float get_der_y_v(const float u, const float v);
        float get_der_z_v(const float u, const float v);

        glm::vec3 get_der_u(const float u, const float v);
        glm::vec3 get_der_v(const float u, const float v);

        glm::vec3 get_normal(const float u, const float v);
};
